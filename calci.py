def calci(x , y, opt):
	if opt == '+':
		return x+y
	elif opt == '-':
		return x-y
	elif opt == '*':
		return x*y
	elif opt == '/':
		return x/y
	else:
		return "Please choose correct operator"

x = input ("Choose value of x: ")
y = input ("Choose value of y: ")
opt = input ("""Choose operator 
1. Addition : +
2. Subtraction : -
3. Multiply : *
4. Divide : /
""")

result= calci(int(x), int(y), opt)
print(result)